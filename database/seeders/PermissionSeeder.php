<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Menu;
use App\Models\SubMenu;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
   {

      $submenu= SubMenu::join('menu', 'submenu.menu_id', '=', 'menu.uuid')
                        ->select('submenu.*', 'menu.name as menu_name')
                        ->orderBy('menu.name', 'ASC')
                        ->orderBy('no_order', 'ASC')
                        ->get();

      foreach($submenu as $submenu){
         Permission::create([
            'name' => str_replace(' ', '', strtolower($submenu->menu_name)).'-'.str_replace(' ', '', strtolower($submenu->name)),
            'menu_name' => str_replace(' ', '', strtolower($submenu->menu_name))
         ]);
      };
      
   }
}
