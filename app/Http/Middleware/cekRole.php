<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App\Helpers\FlashMessages;

class cekRole
{
    use FlashMessages;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        $sessionUser = session('sess_user');
        
        if(!$sessionUser){
            return redirect('/login');
        }

        $roles = explode('|', $role);

        if (in_array($sessionUser->nama_role, $roles)){
            return $next($request);
        }else{
            return redirect('/permission-error');
        }

    }
}
