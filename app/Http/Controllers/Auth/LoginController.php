<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\Menu;
use App\Models\SubMenu;
//use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) 
    {
        $user = User::select('user.*', 'role.name as nama_role')
                ->join('user_has_role', 'user.uuid', '=', 'user_has_role.user_uuid')
                ->join('role', 'role.uuid', '=', 'user_has_role.role_uuid')
                ->whereNull('user.deleted_at')
                ->where('user.email', $request->email)
                ->first();

        if(!$user){
            self::danger('Email is not existed.');
            return redirect()->back();
        }

        $menu = Menu::orderBy('no_order', 'ASC')->get();
        $submenu= SubMenu::join('menu', 'submenu.menu_id', '=', 'menu.uuid')
                        ->select('submenu.*')
                        ->where('status', 1)
                        ->orderBy('no_order', 'ASC')
                        ->get();

        $query = "
                SELECT b.menu_name FROM 
                role_has_permission a
                JOIN permission b ON a.permission_id = b.uuid
                LEFT JOIN role c ON a.role_id = c.uuid
                LEFT JOIN user_has_role d ON c.uuid = d.role_uuid
                LEFT JOIN user e ON d.user_uuid = e.uuid
                WHERE e.email = '".$user->email."'
                GROUP BY b.menu_name
                ";

        $userMenu = DB::select(DB::raw($query));

        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        if($user){
            if (auth()->attempt(['email' => $request->email, 'password' => $request->password]))
            {
                Session::put('sess_menu', $menu);
                Session::put('sess_submenu', $submenu);
                Session::put('sess_usermenu', $userMenu);
                Session::put('sess_user', $user);
                self::success('Successfully login. Welcome back, '.$user->name.'!');
                return redirect()->route('home');
            }else{
                self::danger('Wrong email or password.');
                return redirect()->back();
            }
        }else{
            self::danger('User is not existed.');
            return redirect()->back();
        }
    }

    public function logout(Request $request) 
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/login');
        // Session::flush();
        // Session::regenerate();
        // Auth::logout();
        // self::danger('Successfully logout from system');
        // return redirect('/login');
    }

}
