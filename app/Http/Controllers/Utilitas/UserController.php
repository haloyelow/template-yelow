<?php

namespace App\Http\Controllers\Utilitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\SubMenu;
use App\Models\Role;
use App\Models\UserHasRole;
use App\Models\Permission;
use App\Models\RoleHasPermission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function index()
    {   
        $role = Role::orderBy('name', 'ASC')->get();
        $roles = Role::orderBy('name', 'ASC')->get();
        
        if(request()->ajax())
        {

            $data = User::whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();

            return datatables()->of($data)
                ->addColumn('role', static function ($row) {
                    $roleName = UserHasRole::where('user_uuid', $row->uuid)->first();
                    $role = Role::where('uuid', $roleName->role_uuid)->first();
                    return '<span class="badge bg-info">'.$role->name.'</span>';
                })
                ->addColumn('status', static function ($row) {
                    if($row->status == 1){
                        return '<span class="badge bg-success">Active</span>';
                    }else{
                        return '<span class="badge bg-warning">Suspend</span>';
                    }
                })
                ->addColumn('idUser', static function ($row) {
                    return $row->uuid;
                })
                ->rawColumns(['role', 'status', 'idUser'])
                ->make(true);
        }

        return view('utilitas.user.index', compact('role', 'roles'));
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('utilitas.user.create', compact('role'));
    }

    public function save(Request $request)
    {
        // dd($request->all());
        $role = Role::where('name', $request->role)->first();
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|string|unique:user',
            'password' => 'required|min:6',
            'role' => 'required|string|exists:role,name'
        ]);

        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'status' => 0,
            'is_superadmin' => 0,
            'is_hide' => 0,
            'created_at' => now()
        ]);

        UserHasRole::create([
            'user_uuid' => $user->uuid,
            'role_uuid' => $role->uuid
        ]);

        return response()->json([
			'data' => $user,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
    }

    public function edit($id)
    {
        $user = User::where('uuid', $id)->first();
        return view('utilitas.user.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|string|exists:user,email',
            'password' => 'nullable|min:6',
            'status' => 'required|integer',
        ]);

        $user = User::findOrFail($request->id);
        $password = !empty($request->password) ? bcrypt($request->password):$user->password;
        $user->update([
            'name' => $request->name,
            'password' => $password,
            'status' => $request->status
        ]);
        
        return response()->json([
			'data' => $user,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
    }

    public function delete(Request $request)
    {
        $user = User::findOfFail($request->id);
        $user->update([
			'deleted_by' => session('sess_user')->name
		]);
		$user->delete();

        return response()->json([
			'data' => $user,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully delete data'
		]);
    }

    public function roles(Request $request, $id)
    {
        $user = User::where('uuid', $id)->first();
        $roles = Role::all()->pluck('name');
        $userHasRole = UserHasRole::where('user_uuid', $user->uuid)->first();
        $roleName = Role::where('uuid', $userHasRole->role_uuid)->first()->name;
        return view('utilitas.user.roles', compact('user', 'roles', 'roleName'));
    }

    public function setRole(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);
        
        $user = User::findOrFail($id);
        $role = Role::where('name', $request->role)->first(); 
        $updateRole = UserHasRole::where('user_uuid', $user->uuid)->first();;
        $updateRole->update([
            'user_uuid' => $user->uuid,
            'role_uuid' => $role->uuid
        ]);

        self::success('Data role berhasil diset.');
        return redirect()->back();
    }

    public function updateRole(Request $request)
    {
        $this->validate($request, [
            'roleUpdated' => 'required'
        ]);
        
        $user = User::findOrFail($request->id);
        $role = Role::where('uuid', $request->roleUpdated)->first(); 

        $updateRole = UserHasRole::where('user_uuid', $user->uuid)->first();;
        $updateRole->update([
            'user_uuid' => $user->uuid,
            'role_uuid' => $role->uuid
        ]);

        return response()->json([
			'data' => $updateRole,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update role'
		]);
    }

    public function rolePermission(Request $request)
    {
        $submenu= SubMenu::join('menu', 'submenu.menu_id', '=', 'menu.uuid')
                        ->select('submenu.*', 'menu.name as menu_name')
                        ->orderBy('menu.name', 'ASC')
                        ->orderBy('no_order', 'ASC')
                        ->get();
        $role = $request->get('role');

        $permissions = null;
        $hasPermission = null;
        
        $roles = Role::all()->pluck('name');
        
        if (!empty($role)) {
            $getRole = Role::where('name', $role)->first();
            
            $hasPermission = DB::table('role_has_permission')
            ->select('permission.name')
            ->join('permission', 'role_has_permission.permission_id', '=', 'permission.uuid')
            ->where('role_id', $getRole->uuid)->get()->pluck('name')->all();

            $permissions = Permission::orderBy('name', 'ASC')->pluck('name')->all();
        }
        return view('utilitas.user.role_permission', compact('roles', 'permissions', 'hasPermission', 'submenu'));
    }

    public function addPermission(Request $request)
    { 
        $this->validate($request, [
            'name' => 'required|string|unique:permission'
        ]);

        $explodeMenu = explode('-', $request->name);
        
        $permission = Permission::firstOrCreate([
            'name' => $request->name,
            'menu_name' => $explodeMenu[0]
        ]);

        self::success('Data permission berhasil ditambahkan.');
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        $role = Role::where('name', $role)->first();
        $dataPermission = RoleHasPermission::where('role_id', $role->uuid)->get();

        if($request->has('permission')){
            if(count($dataPermission) == 0){
                foreach($request->permission as $data){
                    $permission = Permission::where('name', $data)->first();
                    RoleHasPermission::create([
                        'permission_id' => $permission->uuid,
                        'role_id' => $role->uuid
                    ]); 
                }
            }else{
                RoleHasPermission::where('role_id', $role->uuid)->delete();
                foreach($request->permission as $data){
                    $permission = Permission::where('name', $data)->first();
                    RoleHasPermission::create([
                        'permission_id' => $permission->uuid,
                        'role_id' => $role->uuid
                    ]); 
                }
            }
        }else{
            RoleHasPermission::where('role_id', $role->uuid)->delete();
        }


        self::success('Data permission berhasil disimpan.');
        return redirect()->back();
    }

    public function getData(Request $request)
	{
        $data = User::select('user.*', 'role.name as roleName', 'role.uuid as idRole')
                ->join('user_has_role', 'user.uuid', '=', 'user_has_role.user_uuid')
                ->join('role', 'role.uuid', '=', 'user_has_role.role_uuid')
                ->whereNull('user.deleted_at')
                ->where('user.uuid', $request->id)
                ->first();
		return response()->json($data);
	}

    public function cekEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();
            if($user){
                return response()->json(true);
            }else{
                return response()->json(false);
            }
    }

    public function cekPassword(Request $request)
    {   
        $user = User::where('email', $request->email)->first();
        if(Hash::check($request->password , $user->password)){
            return response()->json(true);
        }else{
            return response()->json(false);
        }
    }

}