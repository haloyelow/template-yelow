<?php

namespace App\Http\Controllers\Utilitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\SubMenu;

class SubMenuController extends Controller
{

	public function index()
	{
		$menu = Menu::where('is_parent', 1)->orderBy('name', 'ASC')->get();

		if(request()->ajax())
			{

			$data = SubMenu::join('menu', 'submenu.menu_id', '=', 'menu.uuid')
							->Select('submenu.*')
							->whereNull('submenu.deleted_at')
							->where('submenu.no_order', '!=', 0)
							->orderBy('menu.no_order', 'ASC')
							->orderBy('submenu.no_order', 'ASC')->get();

				return datatables()->of($data)
					->addColumn('menu', static function ($row) {
						$menu = Menu::where('uuid', $row->menu_id)->first();
						return $menu->name;
						})
						->addColumn('status', static function ($row) {
						if($row->status == 0){
							return '<span class="badge bg-danger">Inactive</span>';
						}else{
							return '<span class="badge bg-success">Active</span>';
						}
						})
						->addColumn('idSubMenu', static function ($row) {
							return $row->uuid;
						})
						->rawColumns(['menu', 'status', 'idSubMenu'])
						->make(true);
			}

		return view('utilitas.submenu.index', compact('menu'));
	}

	public function save(Request $request)
	{	        
			//validasi data
			$this->validate($request, [
				'menu_id' => 'required|string',
				'name' => 'string|max:100',
				'url' => 'nullable|string|max:100',
				'status' => 'integer',
				'no_order' => 'integer',
				'created_at' => date("Y-m-d H:i:s")
			]);	

			$data = SubMenu::create([
				'menu_id' => $request->menu_id,
				'name' => $request->name,
				'url' => $request->url,
				'status' => $request->status,
				'no_order' => $request->no_order
			]);

			return response()->json([
				'data' => $data,
				'success' => true,
				'alert' => 'success',
				'message' => 'Successfully add data'
			]);
	}

	public function delete(Request $request)
	{  
		$data = SubMenu::where('uuid', $request->id)->first();
		$data->delete();

		return response()->json([
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully delete data'
		]);
	}

	public function update(Request $request)
	{  
		$this->validate($request, [
			'menu_id' => 'required|string',
			'name' => 'string|max:100',
			'url' => 'nullable|string|max:100',
			'status' => 'integer',
			'no_order' => 'integer',
			'updated_at' => date("Y-m-d H:i:s")
		]);
		
		$data = SubMenu::where('uuid', $request->id)->first();

		$data->update([
			'menu_id' => $request->menu_id,
			'name' => $request->name,
			'url' => $request->url,
			'status' => $request->status,
			'no_order' => $request->no_order,
			'updated_at' => date("Y-m-d H:i:s")
		]);

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
	}

	public function getData(Request $request)
	{
		$data = SubMenu::where('uuid', $request->id)->first();
		return response()->json($data);
	}

}