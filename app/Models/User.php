<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Uuid;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property Carbon|null $created_at
 * @property string|null $created_by
 * @property Carbon|null $updated_at
 * @property string|null $updated_by
 * @property Carbon|null $deleted_at
 * @property string|null $deleted_by
 * 
 * @property Collection|Administrator[] $administrators
 * @property Collection|Customer[] $customers
 *
 * @package App\Models
 */
class User extends Authenticatable
{

	public $timestamps = false;
	public $incrementing = false;
	
	use SoftDeletes, Notifiable, HasRoles, Uuid;
	
	protected $table = 'user';
	protected $primaryKey = 'uuid';
	protected $keyType = 'string';
	protected $casts = [
		'uuid' => 'string'
	];
	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'uuid',
		'company_uuid',
		'name',
		'email',
		'password',
		'phone',
		'address',
		'photo',
		'is_superadmin',
		'assign_to',
		'helpdesk_category_id',
		'is_verify',
		'status',
		'bio',
		'url_fb',
		'url_instagram',
		'url_twitter',
		'is_hide',
		'birthdate',
		'gender',
		'token',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	];

	public function getRoleAttribute()
	{
		return session('sess_user')->nama_role;
	}

}
