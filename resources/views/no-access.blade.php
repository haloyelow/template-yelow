@extends('layouts.master')

@section('title')
   <title>Permission Error</title>
@endsection

@section('content')
<!-- ============================================================== -->
         <!-- Start right Content here -->
         <!-- ============================================================== -->
         <div class="main-content">

            <div class="page-content">
               <section class="my-5 pt-sm-5">
                     <div class="container">
                        <div class="row">
                           <div class="col-12 text-center">
                                 <div class="home-wrapper">
                                    <div class="mb-5">
                                       <a href="/home" class="d-block auth-logo">
                                             <img src="{{ asset('backend/assets/images/logo.png') }}" alt="" height="50" class="auth-logo-dark mx-auto">
                                             <img src="{{ asset('backend/assets/images/logo.png') }}" alt="" height="50" class="auth-logo-light mx-auto">
                                       </a>
                                    </div>
                                    <div class="row justify-content-center">
                                       <div class="col-sm-4">
                                             <div class="maintenance-img">
                                                <img src="{{ asset('backend/assets/images/maintenance.svg') }}" alt="" class="img-fluid mx-auto d-block">
                                             </div>
                                       </div>
                                    </div>
                                    <h3 class="mt-2">You have no permission to access this page.</h3>
                                 </div>
                           </div>
                        </div>
                     </div>
               </section>
            </div>
            <!-- End Page-content -->
         </div>
         <!-- end main content-->
@endsection
@section('scripts')
@endsection
