            <!-- ========== Left Sidebar Start ========== -->
            <div class="vertical-menu">

                <div data-simplebar class="h-100">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu list-unstyled" id="side-menu">

                            @foreach(session('sess_menu') as $menu)
                                @foreach(session('sess_usermenu') as $userMenu)
                                    @if($userMenu->menu_name == str_replace(' ', '', strtolower($menu->name)))
                                        <li>
                                            <a href="{{ $menu->url }}" class="@if($menu->is_parent == 1) has-arrow @endif waves-effect">
                                                <i class="{{ $menu->icon }}"></i>
                                                <span key="t-{{ $menu->name }}">{{ $menu->name }}</span>
                                            </a>
                                            @if($menu->is_parent == 1)
                                            <ul class="sub-menu" aria-expanded="false">
                                            @foreach(session('sess_submenu') as $submenu)
                                                @if($submenu->menu_id == $menu->uuid)
                                                    <li><a href="{{ $submenu->url }}" key="t-{{ strtolower($submenu->name) }}">{{ $submenu->name }}</a></li>
                                                    {{-- @if(auth()->user()->can(str_replace(' ', '', strtolower($submenu->menu->name)).'-'.str_replace(' ', '', strtolower($submenu->name))))
                                                    @endif --}}
                                                @endif
                                            @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            @endforeach
                            
                            @if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    <i class="bx bx-cog"></i>
                                    <span key="t-utilitas">Utility</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    <li><a href="{{ route('user.index') }}" key="t-user">User</a></li>
                                    <li><a href="{{ route('role.index') }}" key="t-role">Role</a></li>
                                    <li><a href="{{ route('user.roles_permission') }}" key="t-permission">Permission</a></li>
                                    <li><a href="{{ route('menu.index') }}" key="t-menu">Menu</a></li>
                                    <li><a href="{{ route('submenu.index') }}" key="t-submenu">Submenu</a></li>
                                </ul>
                            </li>
                            @endif
                            <li>
                                <a href="javascript: void(0);" onclick="konfirmasiLogout(event)" class="waves-effect">
                                    <i class="bx bx-power-off"></i>
                                    <span key="t-logout">Logout</span>
                                </a>
                                <form id="logout-form" action="javascript: void(0);" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>

                        </ul>
                    </div>
                    <!-- Sidebar -->
                </div>
            </div>
            <!-- Left Sidebar End -->