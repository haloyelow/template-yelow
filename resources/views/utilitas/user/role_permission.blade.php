@extends('layouts.master')

@section('title')
    <title>Role Permission</title>
@endsection

@section('content')
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Role Permission</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Utilitas</a></li>
                                        <li class="breadcrumb-item active">Permission</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    @include ('partials.messages')
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h4 class="card-title">Add New Pemission</h4><br>
                                            <form role="form" action="{{ route('user.add_permission') }}" method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div>
                                                            <label class="form-label">List Sub Menu</label>
                                                            <select name="name" id="name" required class="form-select {{ $errors->has('name') ? 'is-invalid':'' }}">
                                                                @foreach ($submenu as $row)
                                                                    <option value="{{ str_replace(' ', '', strtolower($row->menu_name)) }}-{{ str_replace(' ', '', strtolower($row->name)) }}">{{ $row->menu_name }} - {{ ucfirst($row->name) }}</option>
                                                                @endforeach
                                                            </select>
                                                            <p class="text-danger">{{ $errors->first('name') }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <a href="/home" class="btn btn-light">Kembali</a>
                                            </form>
                                        </div>
                                        <div class="col-md-8">
                                            <h4 class="card-title">Set Permission to Role</h4><br>
                                            <form role="form" action="{{ route('user.roles_permission') }}" method="GET">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div>
                                                            <label class="form-label">Roles</label>
                                                            <select name="role" class="form-select">
                                                                @foreach ($roles as $value)
                                                                    <option value="{{ $value }}" {{ request()->get('role') == $value ? 'selected':'' }}>{{ $value }}</option>
                                                                @endforeach
                                                            </select><p></p>
                                                        </div>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success">Check!</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </form><br>
                                            @if (!empty($permissions))
                                                <form action="{{ route('user.setRolePermission', request()->get('role')) }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <div class="col-lg-8">
                                                        <div class="nav-tabs-custom">
                                                            <ul class="nav nav-tabs">
                                                                <li class="active">
                                                                    <a href="#tab_1" data-toggle="tab">Sub Menu Set For Permission</a>
                                                                </li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div class="tab-pane active" id="tab_1">
                                                                    @php $no = 1; @endphp
                                                                    @foreach ($permissions as $key => $row)
                                                                        <input type="checkbox"
                                                                            name="permission[]"
                                                                            class="minimal-red"
                                                                            value="{{ $row }}"
                                                                            {{--  CHECK, JIKA PERMISSION TERSEBUT SUDAH DI SET, MAKA CHECKED --}}
                                                                            {{ in_array($row, $hasPermission) ? 'checked':'' }}
                                                                            > {{ $row }} <br>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><p>`</p>
                                                    <div class="pull-right">
                                                        <button class="btn btn-primary btn-sm">
                                                            <i class="fa fa-send"></i> Set Permission
                                                        </button>
                                                    </div>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->

                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
        </div>
        <!-- end main content-->
@endsection
