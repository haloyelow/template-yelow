  @extends('layouts.master')

  @section('title')
      <title>Tambah User</title>
  @endsection

  @section('content')
  <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

              <div class="page-content">
                  <div class="container-fluid">

                      <!-- start page title -->
                      <div class="row">
                          <div class="col-12">
                              <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                  <h4 class="mb-sm-0 font-size-18">Tambah User</h4>

                                  <div class="page-title-right">
                                      <ol class="breadcrumb m-0">
                                          <li class="breadcrumb-item"><a href="javascript: void(0);">Utilitas</a></li>
                                          <li class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></li>
                                          <li class="breadcrumb-item active">Tambah User</li>
                                      </ol>
                                  </div>

                              </div>
                          </div>
                      </div>
                      <!-- end page title -->

                      <div class="row">
                          <div class="col-lg-12">
                            <form action="{{ route('user.store') }}" method="post">
                              @csrf
                              <div class="card">
                                  <div class="card-body">
                                      <div>
                                          <div class="row">
                                              <div class="col-lg-8">
                                                  <div>
                                                      <label class="form-label">Name</label>
                                                      <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" required>
                                                      <p class="text-danger">{{ $errors->first('name') }}</p>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-lg-8">
                                                <div>
                                                    <label class="form-label">Email</label>
                                                    <input class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" type="email" name="email" required>
                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-lg-8">
                                                <div>
                                                    <label class="form-label">Password</label>
                                                    <input class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" type="password" name="password" required>
                                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-lg-8">
                                                <div>
                                                    <label class="form-label">Role</label>
                                                    <select class="form-select {{ $errors->has('role') ? 'is-invalid':'' }}" name="role" required>
                                                        <option value="">Pilih</option>
                                                        @foreach ($role as $row)
                                                        <option value="{{ $row->name }}">{{ $row->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <p class="text-danger">{{ $errors->first('role') }}</p>
                                                </div>
                                            </div>
                                          </div>
                                          <button type="submit" class="btn btn-primary">Submit</button>
                                          <a href="{{ url()->previous() }}" class="btn btn-warning">Cancel</a>
                                      </div>
                                  </div>
                              </div>
                            </form>
                          </div>
                      </div>
                      <!-- end row -->

                  </div> <!-- container-fluid -->
              </div>
              <!-- End Page-content -->
            </div>
            <!-- end main content-->
  <!-- /.control-sidebar -->
  @endsection
