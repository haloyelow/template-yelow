const token = $('meta[name="csrf-token"]').attr('content')
const routeGetDataCompanyCategory = $("#routeGetDataCompanyCategory").val()
const routeGetDataHelpdeskCategory = $("#routeGetDataHelpdeskCategory").val()
const routeSaveDataCompanyCategory = $("#routeSaveDataCompanyCategory").val()
const routeSaveDataHelpdeskCategory = $("#routeSaveDataHelpdeskCategory").val()

tableCompany();

function tableCompany() {
    var dataTableCompany = $("#table-company").DataTable({
        language: {
            paginate: {
                next: '<i class="fas fa-chevron-right"></i>',
                previous: '<i class="fas fa-chevron-left"></i>'
            }
        },
        processing: false,
        bLengthChange: false,
        bInfo: false,
        pageLength: 10,
        ajax: {
            url: "/master/category/company",
        },
        stripeClasses: ['stripe-color', 'stripe-2'],
        columns: [
            { data: "name" },
            { data: "description" },
            {
                data: "action",
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    var id = row.id;
                    return (
                        `
                            <div class="d-flex">
                                <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="editCompany(event)"><i class="fas fa-pen"></i></a>
                                <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapusCompany(event)"><i class="fa fa-trash"></i></a>                            
                            </div>
                        `
                    );
                },
            },
        ],
        searchDelay: 750,
        buttons: [],
        columnDefs: [
            {
                defaultContent: "-",
                targets: "_all",
                className: "text-left",
            },
        ],
        bDestroy: true,
    });
    $(".dataTables_filter").hide();

    $("#btnSearchCompany").click(function() {
        let valInput = document.getElementById("inputSeacrhCompany").value;
        dataTableCompany.search(valInput).draw();
    });
}

function tableHelpdesk() {
    var dataTableHelpdesk = $("#table-helpdesk").DataTable({
        language: {
            paginate: {
                next: '<i class="fas fa-chevron-right"></i>',
                previous: '<i class="fas fa-chevron-left"></i>'
            }
        },
        processing: false,
        bLengthChange: false,
        bInfo: false,
        pageLength: 10,
        ajax: {
            url: "/master/category/helpdesk",
        },
        columns: [
            { data: "name" },
            {
                data: "action",
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    var id = row.id;
                    return (
                        `
                            <div class="d-flex">
                                <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="editHelpdesk(event)"><i class="fas fa-pen"></i></a>
                                <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapusHelpdesk(event)"><i class="fa fa-trash"></i></a>
                            </div>
                        `
                    );
                },
            },
        ],
        searchDelay: 750,
        buttons: [],
        columnDefs: [
            {
                defaultContent: "-",
                targets: "_all",
                className: "text-left",
            },
        ],
        bDestroy: true,
    });
    $(".dataTables_filter").hide();

    $("#btnSearchHelpdesk").click(function name() {
        let valInput = document.getElementById("inputSeacrhHelpdesk").value;
        dataTableHelpdesk.search(valInput).draw();
    });
}

$("#showCompany").on("click", function (e) {
    document.title = 'Company Category'
    $("#tab-company").show();
    $(".tab-helpdesk").hide();
    tableCompany();
});
$("#showHelpdesk").on("click", function (e) {
    document.title = 'Helpdesk Category'
    $("#tab-company").hide();
    $(".tab-helpdesk").show();
    tableHelpdesk();
});

function editCompany(ev) {
    document.title = 'Edit Company Category'
    ev.preventDefault();
    let idCompany = ev.currentTarget.getAttribute('id');
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: routeGetDataCompanyCategory,
        data: {id: idCompany},
        success: function (response) {
            const routeEdit = "/master/category/company/edit/"+idCompany 
            $('#routeSaveDataCompanyCategory').val(routeEdit)
            $('#companyName').val(response.name)
            $('#companyDesc').val(response.description)
        }
    })
}

function editHelpdesk(ev) {
    document.title = 'Edit Helpdesk Category'
    ev.preventDefault();
    let idHelpdek = ev.currentTarget.getAttribute('id');
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: routeGetDataHelpdeskCategory,
        data: {id: idHelpdek},
        success: function (response) {
            const routeEdit = "/master/category/helpdesk/edit/"+idHelpdek 
            $('#routeSaveDataHelpdeskCategory').val(routeEdit)
            $('#helpdeskName').val(response.name)
        }
    })
}

$('#btnSaveCompany').on('click', function (e) {
    e.preventDefault()
    const routeSaveDataCompanyCategory = $("#routeSaveDataCompanyCategory").val()
    let idCompany = routeSaveDataCompanyCategory.substring(routeSaveDataCompanyCategory.lastIndexOf('/') + 1)
    if(idCompany.length >= 7){
        var url = "/master/category/company/update"
        var data = {
            id:idCompany,
            name:$("#companyName").val(),
            description:$("#companyDesc").val(),
        } 
    }else{
        var url = "/master/category/company/save"
        var data = {
            name:$("#companyName").val(),
            description:$("#companyDesc").val(),
        }
    }
            
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: url,
        data: data,
        success: function (response) {
            document.title = 'Company Category'
            document.getElementById("formInputCompany").reset(); 
            const notifAlert = `
                <div class="col-md-12 div`+response.data.uuid+`">
                    <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                </div>
            `
            $('#notifCompany').append(notifAlert)
            setTimeout(function() {
                $('.div'+response.data.uuid).fadeOut('slow')
            }, 1500)
            tableCompany()
            const route = "/master/category/company/save" 
            $('#routeSaveDataCompanyCategory').val(route)
        },
        error: function (xhr) {
            var err = JSON.parse(xhr.responseText)
            var errorString = '<ul>'
            $.each( err.errors, function( key, value) {
                errorString += '<p>' + value + '</p>'
            })
            errorString += '</ul>'
            
            swal({
                type: "warning",
                title: errorString,
                showCancelButton: false,
                confirmButtonColor: "#e3342f",
                confirmButtonText: "Ok",
            })
        }
    })
});

$('#btnClearCompany').on('click', function (e) {    
    document.title = 'Company Category'
    $('#companyName').val('')
    $('#companyDesc').val('')
    const route = "/master/category/company/save" 
    $('#routeSaveDataCompanyCategory').val(route)
});

$('#btnSaveHelpdesk').on('click', function (e) {
    e.preventDefault()
    const routeSaveDataHelpdeskCategory = $("#routeSaveDataHelpdeskCategory").val()
    let idHelpdek = routeSaveDataHelpdeskCategory.substring(routeSaveDataHelpdeskCategory.lastIndexOf('/') + 1)
    if(idHelpdek.length >= 7){
        var url = "/master/category/helpdesk/update"
        var data = {
            id:idHelpdek,
            name:$("#helpdeskName").val(),
        } 
        var title = "Successfully update data"
    }else{
        var url = "/master/category/helpdesk/save"
        var data = {
            name:$("#helpdeskName").val(),
        }
        var title = "Successfully add data"
    }
    
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: url,
        data: data,
        success: function (response) {
            document.title = 'Company Category'
            document.getElementById("formInputHelpdesk").reset()
            const notifAlert = `
                <div class="col-md-12 div`+response.data.uuid+`">
                    <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                </div>
            `
            $('#notifHelpdesk').append(notifAlert)
            setTimeout(function() {
                $('.div'+response.data.uuid).fadeOut('slow')
            }, 1500)
            tableHelpdesk()
            const route = "/master/category/helpdesk/save" 
            $('#routeSaveDataHelpdeskCategory').val(route) 
        },
        error: function (xhr) {
            var err = JSON.parse(xhr.responseText)
            var errorString = '<ul>'
            $.each( err.errors, function( key, value) {
                errorString += '<p>' + value + '</p>'
            })
            errorString += '</ul>'
            
            swal({
                type: "warning",
                title: errorString,
                showCancelButton: false,
                confirmButtonColor: "#e3342f",
                confirmButtonText: "Ok",
            })
        }
    })
});

$('#btnClearHelpdesk').on('click', function (e) {  
    document.title = 'Helpdesk Category'  
    $('#helpdeskName').val('')
    const route = "/master/category/helpdesk/save" 
    $('#routeSaveDataHelpdeskCategory').val(route)
});

function hapusCompany(ev) {
    ev.preventDefault();
    let idCompany = ev.currentTarget.getAttribute('id');
    swal({
        title: "Are you sure want to delete this data?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                headers: { 'X-CSRF-TOKEN': token },
                type: 'GET',
                url: "/master/category/company/delete/",
                data: {id: idCompany},
                success: function (response) {
                    const notifAlert = `
                        <div class="col-md-12 div`+response.data.uuid+`">
                            <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                        </div>
                    `
                    $('#notifCompany').append(notifAlert)
                    setTimeout(function() {
                        $('.div'+response.data.uuid).fadeOut('slow')
                    }, 1500)
                    tableCompany()
                }
            })
        } else {
            swal("Hapus gagal.");
        }
    });
}

function hapusHelpdesk(ev) {
    ev.preventDefault();
    let idHelpdek = ev.currentTarget.getAttribute('id');
    swal({
        title: "Are you sure want to delete this data?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                headers: { 'X-CSRF-TOKEN': token },
                type: 'GET',
                url: "/master/category/helpdesk/delete/",
                data: {id: idHelpdek},
                success: function (response) {
                    const notifAlert = `
                        <div class="col-md-12 div`+response.data.uuid+`">
                            <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                        </div>
                    `
                    $('#notifHelpdesk').append(notifAlert)
                    setTimeout(function() {
                        $('.div'+response.data.uuid).fadeOut('slow')
                    }, 1500)
                    tableHelpdesk()
                }
            })
        } else {
            swal("Hapus gagal.");
        }
    });
}