const token = $('meta[name="csrf-token"]').attr('content')
const routeGetData = $("#routeGetData").val()
// const routeSaveData = $("#routeSaveData").val()
let accordionOne = document.getElementById("flush-collapseOne")
let accordionTwo = document.getElementById("flush-collapseTwo")

displayData()

function displayData(){
   var dataTable = $("#table-menu").DataTable({
      language: {
         paginate: {
               next: '<i class="fas fa-chevron-right"></i>',
               previous: '<i class="fas fa-chevron-left"></i>'
         }
      },
      processing: false,
      bLengthChange: false,
      bInfo: false,
      pageLength: 10,
      stripeClasses: ['stripe-color', 'stripe-2'],
      ajax: {
         url: "/utilitas/menu",
      },
      columns: [
         { data: "name" },
         { data: "icon" },
         { data: "url" },
         { data: "is_parent" },
         { data: "no_order" },
         {
               data: "action",
               orderable: false,
               searchable: false,
               render: function (data, type, row) {
                  var id = row.idMenu;
                  return (
                     `
                        <div class="d-flex">
                           <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="edit(event)"><i class="fas fa-pen"></i></a>
                           <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                        </div>
                     `
                  );
               },
         },
      ],
      searchDelay: 750,
      buttons: [],
      columnDefs: [
         {
               defaultContent: "-",
               targets: "_all",
               className: "text-left",
         },
      ],
      bDestroy: true,
      order: [[5, "ASC"]],
   });

   $(".dataTables_filter").hide();
   
   $("#btnSearch").click(function name() {
      let valInput = document.getElementById("myInputTextField").value;
      dataTable.search(valInput).draw();
   });
}

function edit(ev) {
   document.title = 'Edit Menu'
   accordionOne.classList.add("show")
   accordionTwo.classList.remove("show")

   ev.preventDefault();
   let idMenu = ev.currentTarget.getAttribute('id');
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeGetData,
      data: {id: idMenu},
      success: function (response) {
         $('#idEdit').val(idMenu)
         $('#name').val(response.name)
         $('#icon').val(response.icon)
         $('#url').val(response.url)
         $('#is_parent').val(response.is_parent)
         $('#no_order').val(response.no_order)
      }
   })
}

$('#btnSubmit').on('click', function (e) {
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
   e.preventDefault()
   let idMenu = $('#idEdit').val()

   if(idMenu.length >= 7){
      var url = "/utilitas/menu/update"
      var data = {
               name:$("#name").val(),
               icon:$("#icon").val(),
               url:$("#url").val(),
               is_parent:$("#is_parent").val(),
               no_order:$("#no_order").val(),
               id:idMenu
         } 
   }else{
      var url = "/utilitas/menu/save"
      var data = {
               name:$("#name").val(),
               icon:$("#icon").val(),
               url:$("#url").val(),
               is_parent:$("#is_parent").val(),
               no_order:$("#no_order").val(),                
         }
   }

   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: url,
      data: data,
      success: function (response) {
         document.title = 'Data Menu'
         document.getElementById("formInput").reset()
         const notifAlert = `
                              <div class="col-md-12 div`+response.data.uuid+`">
                                 <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                              </div>
                           `
         $('#notif').append(notifAlert)
         setTimeout(function() {
               $('.div'+response.data.uuid).fadeOut('slow')
         }, 1500)
         displayData()
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});

$('#btnClear').on('click', function (e) {
   document.title = 'Data Menu'
   $('#idEdit').val('')
   document.getElementById("formInput").reset()
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
});

$('#btnAdd').on('click', function (e) {
   document.title = 'Add Menu'
   accordionOne.classList.add("show")
   accordionTwo.classList.remove("show")
});

$('.btnForm').on('click', function(e){
   document.title = 'Add Menu'
})

$('.btnList').on('click', function(e){
   document.title = 'Data Menu'
})

function hapus(ev) {
   ev.preventDefault();
   let idMenu = ev.currentTarget.getAttribute('id');
   swal({
      title: "Are you sure want to delete this data?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
   })
   .then((willDelete) => {
      if (willDelete) {
         $.ajax({
               headers: { 'X-CSRF-TOKEN': token },
               type: 'GET',
               url: "/utilitas/menu/delete/",
               data: {id: idMenu},
               success: function (response) {
                  const notifAlert = `
                           <div class="col-md-12 div`+response.data.uuid+`">
                              <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                           </div>
                     `
                  $('#notif').append(notifAlert)
                  setTimeout(function() {
                     $('.div'+response.data.uuid).fadeOut('slow')
                  }, 1500)
                  displayData()
               }
         })
      } else {
         swal("Hapus gagal.");
      }
   });
}