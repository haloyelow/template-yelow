const token = $('meta[name="csrf-token"]').attr('content')
const routeGetDataBank = $("#routeGetDataBank").val()
const routeSaveData = $("#routeSaveData").val()

displayData()

function displayData(){
    var dataTable = $("#table-bank").DataTable({
        language: {
            paginate: {
                next: '<i class="fas fa-chevron-right"></i>',
                previous: '<i class="fas fa-chevron-left"></i>'
            }
        },
        processing: false,
        bLengthChange: false,
        bInfo: false,
        pageLength: 10,
        stripeClasses: ['stripe-color', 'stripe-2'],
        ajax: {
            url: "/master/bank",
        },
        columns: [
            { data: "name" },
            {
                data: "action",
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    var id = row.idBank;
                    return (
                        `
                    <div class="d-flex">
                        <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="edit(event)"><i class="fas fa-pen"></i></a>
                        <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                    </div>
                            
                        `
                    );
                },
            },
        ],
        searchDelay: 750,
        buttons: [],
        columnDefs: [
            {
                defaultContent: "-",
                targets: "_all",
                className: "text-left",
            },
        ],
        bDestroy: true,
    });

    $(".dataTables_filter").hide();
    
    $("#btnSearch").click(function name() {
        let valInput = document.getElementById("myInputTextField").value;
        dataTable.search(valInput).draw();
    });
}

function edit(ev) {
    document.title = 'Edit Bank'
    ev.preventDefault();
    let idBank = ev.currentTarget.getAttribute('id');
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: routeGetDataBank,
        data: {id: idBank},
        success: function (response) {
            const routeEdit = "/master/bank/edit/"+idBank 
            $('#routeSaveData').val(routeEdit)
            $('#namaBank').val(response.name)
        }
    })
}

$('#btnSave').on('click', function (e) {
    e.preventDefault()
    const routeSaveData = $("#routeSaveData").val()
    let idBank = routeSaveData.substring(routeSaveData.lastIndexOf('/') + 1)

    if(idBank.length >= 7){
        var url = "/master/bank/update"
        var data = {
                name:$("#namaBank").val(),
                id:idBank
            } 
    }else{
        var url = "/master/bank/save"
        var data = {
                name:$("#namaBank").val()                
            }
    }

    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: url,
        data: data,
        success: function (response) {
            document.title = 'Data Bank'
            document.getElementById("formInput").reset()
            const notifAlert = `
            <div class="col-md-12 div`+response.data.uuid+`">
                                    <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                                    </div>
                                    `
            $('#notif').append(notifAlert)
            setTimeout(function() {
                $('.div'+response.data.uuid).fadeOut('slow')
            }, 1500)
            displayData()
            const route = "/master/bank/save" 
            $('#routeSaveData').val(route)
        },
        error: function(xhr) {
            var err = JSON.parse(xhr.responseText)
            var errorString = '<ul>'
            $.each( err.errors, function( key, value) {
                errorString += '<p>' + value + '</p>'
            })
            errorString += '</ul>'
            
            swal({
                type: "warning",
                title: errorString,
                showCancelButton: false,
                confirmButtonColor: "#e3342f",
                confirmButtonText: "Ok",
            })
        }
    })    
});

$('#btnClear').on('click', function (e) {
    document.title = 'Data Bank'
    $('#namaBank').val('')
    const route = "/master/bank/save" 
    $('#routeSaveData').val(route)
});

function hapus(ev) {
    ev.preventDefault();
    let idBank = ev.currentTarget.getAttribute('id');
    swal({
        title: "Are you sure want to delete this data?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                headers: { 'X-CSRF-TOKEN': token },
                type: 'GET',
                url: "/master/bank/delete/",
                data: {id: idBank},
                success: function (response) {
                    const notifAlert = `
                            <div class="col-md-12 div`+response.data.uuid+`">
                                <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                            </div>
                        `
                    $('#notif').append(notifAlert)
                    setTimeout(function() {
                        $('.div'+response.data.uuid).fadeOut('slow')
                    }, 1500)
                    displayData()
                }
            })
        } else {
            swal("Hapus gagal.");
        }
    });
}