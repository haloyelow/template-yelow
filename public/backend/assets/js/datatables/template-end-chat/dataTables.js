const token = $('meta[name="csrf-token"]').attr('content')
const routeGetData = $("#routeGetData").val()
const routeSave = $("#routeSave").val()
const routeUpdate = $("#routeUpdate").val()
const routeDelete = $("#routeDelete").val()
const routeStatus = $("#routeStatus").val()

displayData()

function displayData(){
   var dataTable = $("#table-template-end-chat").DataTable({
      language: {
         paginate: {
               next: '<i class="fas fa-chevron-right"></i>',
               previous: '<i class="fas fa-chevron-left"></i>'
         }
      },
      processing: false,
      bLengthChange: false,
      bInfo: false,
      pageLength: 10,
      stripeClasses: ['stripe-color', 'stripe-2'],
      ajax: {
         url: "/master/template-end-chat/",
      },
      columns: [
         { data: "description" },
         { data: "status" },
         {
               data: "action",
               orderable: false,
               searchable: false,
               render: function (data, type, row) {
                  var id = row.idTemplate;
                  var statusTemplate = row.statusTemplate;
                  return (
                     `
                        <div class="d-flex">
                           <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="edit(event)"><i class="fas fa-pen"></i></a>
                           <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                           <a class="btn-circle text-success" href="javascript:void(0)" id="`+id+`" data-status="`+statusTemplate+`" onclick="status(event)"><i class="fa fa-power-off"></i></a>
                        </div>
                     `
                  );
               },
         },
      ],
      searchDelay: 750,
      buttons: [],
      columnDefs: [
         {
               defaultContent: "-",
               targets: "_all",
               className: "text-left",
         },
      ],
      bDestroy: true,
      // order: [[5, "ASC"]],
   });

   $(".dataTables_filter").hide();
   
   $("#btnSearch").click(function name() {
      let valInput = document.getElementById("myInputTextField").value;
      dataTable.search(valInput).draw();
   });
}

$('#btnAdd').on('click', function (e) {
   $('#modalForm').modal('show') 
   document.title = 'Add Template'
});

$('#modalForm').on('hidden.bs.modal', function () {
   $('#fieldDescription').val('')
   document.title = 'Template End Chat'
   document.getElementById('myModalLabel').innerHTML = 'Add Template'
});

$('#fieldDescription').on('change', function(e){
   $('#btnSubmit').prop("disabled", false)
   $('#btnSubmit').focus()
})

function edit(ev) {
   document.getElementById('myModalLabel').innerHTML = 'Edit Template End Chat'
   document.title = 'Edit Template End Chat'
   $('#modalForm').modal('show')
   
   ev.preventDefault();
   let idTemplate = ev.currentTarget.getAttribute('id')
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeGetData,
      data: {id: idTemplate},
      success: function (response) {
         $('#idEdit').val(idTemplate)
         $('#fieldDescription').val(response.description)
         $('#fieldStatus').val(response.status)
      }
   })
}

$('#btnSubmit').on('click', function (e) {
   e.preventDefault()
   let idTemplate = $('#idEdit').val()
   
   if(idTemplate.length >= 7){
      var url = routeUpdate
      var data = {
         description:$("#fieldDescription").val(),
         status:$('#fieldStatus').val(),
         id:idTemplate
      } 
   }else{
      var url = routeSave
      var data = {
         description:$("#fieldDescription").val(),
         status:$('#fieldStatus').val()
      }
   }
   
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: url,
      data: data,
      success: function (response) {
         document.getElementById('myModalLabel').innerHTML = 'Add Template End Chat'
         document.title = 'Template End Chat'
         $('#idEdit').val('')

         document.getElementById("formInput").reset()
         $('#modalForm').modal('hide') 
         const notifAlert = `
                              <div class="col-md-12 div`+response.data.uuid+`">
                                 <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                              </div>
                           `
         $('#notif').append(notifAlert)
         setTimeout(function() {
               $('.div'+response.data.uuid).fadeOut('slow')
         }, 1500)
         displayData()
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});

function status(ev)
{  
   ev.preventDefault();
   let idTemplate = ev.currentTarget.getAttribute('id')
   let statusTemplate = ev.currentTarget.getAttribute('data-status')
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeStatus,
      data: {id: idTemplate},
      beforeSubmit : function(response){
         //check status active
      },
      success: function (response) {         
         // location.reload()
         if(response.count == 0 && response.data.status == 1){
            // console.log(response.count, response.data.status)
            swal({ 
               type: "success",
               title: "Template is Active",
            })
         }else if(response.count == 1 && response.data.status == 0){
            if(statusTemplate == 1){       
               swal({
                  type: "info",
                  title: "Template is Suspend",
               })
            }else {
               swal({
                  type:"error",
                  title: "You already have an active template",
               })
            }
         }

         displayData()
      },
      error: function(xhr) {
         swal({
               type: "warning",
               title: "Something Wrong!",
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }        
   })
}

$('#btnSubmit').on('click', function (e) {
   e.preventDefault()
   let idTemplate = $('#idEdit').val()
   if(idTemplate.length >= 7){
      var url = routeStatus
      var data = {
         status:$('#fieldStatus').val(),
         id:idTemplate
      } 
   }else{
      var url = routeStatus
      var data = {
         status:$('#fieldStatus').val(),
      }
   }
   
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: url,
      data: data,
      success: function (response) {
         document.getElementById('myModalLabel').innerHTML = 'Change Status'
         document.title = 'Template End Chat'
         $('#idEdit').val('')

         document.getElementById("formInput").reset()
         $('#modalForm').modal('hide') 
         // const notifAlert = `
         //                      <div class="col-md-12 div`+response.data.uuid+`">
         //                         <div class="alert alert-`+response.alert+`">`+response.message+`</div>
         //                      </div>
         //                   `
         // $('#notif').append(notifAlert)
         // setTimeout(function() {
         //       $('.div'+response.data.uuid).fadeOut('slow')
         // }, 1500)
         displayData()
      }
   })
});

// $('#btnSubmit').on('click', function (e) {
//    e.preventDefault()
//    let idTemplate = $('#idEdit').val()
   
//    if(idTemplate.length >= 7){
//       var url = routeUpdate
//       var data = {
//          status:$("#fieldStatus").val(),
//          id:idTemplate
//       } 
//    }else{
//       var url = routeStatus
//       var data = {
//          status:$("#fieldStatus").val(),
//       }
//    }
   
//    $.ajax({
//       headers: { 'X-CSRF-TOKEN': token },
//       type: 'POST',
//       url: url,
//       data: data,
//       success: function (response) {
//                $('#notif').append(notifAlert)
//          setTimeout(function() {
//                $('.div'+response.data.uuid).fadeOut('slow')
//          }, 1500)
//          displayData()
//       }
//       // },
//       // error: function(xhr) {
//       //    var err = JSON.parse(xhr.responseText)
//       //    var errorString = '<ul>'
//       //    $.each( err.errors, function( key, value) {
//       //          errorString += '<p>' + value + '</p>'
//       //    })
//       //    errorString += '</ul>'
         
//       //    swal({
//       //          type: "warning",
//       //          title: errorString,
//       //          showCancelButton: false,
//       //          confirmButtonColor: "#e3342f",
//       //          confirmButtonText: "Ok",
//       //    })
//       // }
//    })
// });

function hapus(ev) {
   ev.preventDefault();
   let idTemplate = ev.currentTarget.getAttribute('id');
   swal({
      title: "Are you sure want to delete this data?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
   })
   .then((willDelete) => {
      if (willDelete) {
         $.ajax({
               headers: { 'X-CSRF-TOKEN': token },
               type: 'GET',
               url: routeDelete,
               data: {id: idTemplate},
               success: function (response) {
                  const notifAlert = `
                           <div class="col-md-12 div`+response.data.uuid+`">
                              <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                           </div>
                     `
                  $('#notif').append(notifAlert)
                  setTimeout(function() {
                     $('.div'+response.data.uuid).fadeOut('slow')
                  }, 1500)
                  displayData()
               }
         })
      } else {
         swal("Hapus gagal.");
      }
   });
}