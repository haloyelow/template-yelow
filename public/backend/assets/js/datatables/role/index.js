const token = $('meta[name="csrf-token"]').attr('content')
const routeGetData = $("#routeGetData").val()

displayData()

function displayData(){
   var dataTable = $("#table-role").DataTable({
      language: {
         paginate: {
               next: '<i class="fas fa-chevron-right"></i>',
               previous: '<i class="fas fa-chevron-left"></i>'
         }
      },
      processing: false,
      bLengthChange: false,
      bInfo: false,
      pageLength: 10,
      stripeClasses: ['stripe-color', 'stripe-2'],
      ajax: {
         url: "/utilitas/role",
      },
      columns: [
         { data: "name" },
         {
               data: "action",
               orderable: false,
               searchable: false,
               render: function (data, type, row) {
                  var id = row.id;
                  return (
                     `
                        <div class="d-flex">
                           <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                        </div>
                     `
                  );
               },
         },
      ],
      searchDelay: 750,
      buttons: [],
      columnDefs: [
         {
               defaultContent: "-",
               targets: "_all",
               className: "text-left",
         },
      ],
      bDestroy: true,
      order: [[1, "ASC"]],
   });

   $(".dataTables_filter").hide();
   
   $("#btnSearch").click(function name() {
      let valInput = document.getElementById("myInputTextField").value;
      dataTable.search(valInput).draw();
   });
}

$('#btnSave').on('click', function (e) {
   e.preventDefault()
   var url = "/utilitas/role/save"
   var data = {
            name:$("#roleName").val(),
      }

   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: url,
      data: data,
      success: function (response) {
         document.getElementById("formInput").reset()
         const notifAlert = `
                              <div class="col-md-12 div`+response.data.uuid+`">
                                 <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                              </div>
                           `
         $('#notif').append(notifAlert)
         setTimeout(function() {
               $('.div'+response.data.uuid).fadeOut('slow')
         }, 1500)
         displayData()
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});

$('#btnClear').on('click', function (e) {
   document.getElementById("formInput").reset()
});

function hapus(ev) {
   ev.preventDefault();
   let idRole = ev.currentTarget.getAttribute('id');
   swal({
      title: "Are you sure want to delete this data?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
   })
   .then((willDelete) => {
      if (willDelete) {
         $.ajax({
               headers: { 'X-CSRF-TOKEN': token },
               type: 'GET',
               url: "/utilitas/role/delete/",
               data: {id: idRole},
               success: function (response) {
                  const notifAlert = `
                           <div class="col-md-12 div`+response.data.uuid+`">
                              <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                           </div>
                     `
                  $('#notif').append(notifAlert)
                  setTimeout(function() {
                     $('.div'+response.data.uuid).fadeOut('slow')
                  }, 1500)
                  displayData()
               }
         })
      } else {
         swal("Hapus gagal.");
      }
   });
}