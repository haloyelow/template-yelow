var dataTable = $("#table-information-banner").DataTable({
    language: {
        paginate: {
            next: '<i class="fas fa-chevron-right"></i>',
            previous: '<i class="fas fa-chevron-left"></i>'
        }
    },
    processing: false,
    bLengthChange: false,
    bInfo: false,
    pageLength: 10,
    ajax: {
        url: "/package/information-banner",
    },
    columns: [
        { data: "name" },
        { data: "image" },
        { data: "no_order" },
        {
            data: "action",
            orderable: false,
            searchable: false,
            render: function (data, type, row) {
                var id = row.id;
                return (
                    `
                        <div class="d-flex">
                            <a class="btn-circle text-primary me-1" href="/package/information-banner/`+id+`/edit"><i class="fas fa-pen"></i></a>
                            <a class="btn-circle text-danger" href="/package/information-banner/delete/`+id+`" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                        </div>
                    `
                );
            },
        },
    ],
    searchDelay: 750,
    buttons: [],
    columnDefs: [
        {
            defaultContent: "-",
            targets: "_all",
            className: "text-left",
        },
    ],
});
$(".dataTables_filter").hide();

$("#btnSearch").click(function name() {
    let valInput = document.getElementById("myInputTextField").value;
    dataTable.search(valInput).draw();
});