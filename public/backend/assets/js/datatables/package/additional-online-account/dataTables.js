var dataTable = $("#table-additional-online-account").DataTable({
    language: {
        paginate: {
            next: '<i class="fas fa-chevron-right"></i>',
            previous: '<i class="fas fa-chevron-left"></i>'
        }
    },
    processing: false,
    bLengthChange: false,
    bInfo: false,
    pageLength: 10,
    ajax: {
        url: "/package/additional-online-account",
        },
    columns: [
        { data: "name" },
        { data: "price" },
        {
            orderable:false,
            data:null,
            render: function (data, type, row) {
                return `
                    <a href="#" class="badge bg-warning buttonExpand`+row.id+`" style="background-color: #F6A500; border:none" id="btnExpand"><span class="expand`+row.id+`">Klik untuk detail</span></a>
                    `;
            }
        },
        {
            data: "action",
            orderable: false,
            searchable: false,
            render: function (data, type, row) {
                var id = row.id;
                return (
                    `
                        <div class="d-flex">
                            <a class="btn-circle text-primary me-1" href="/package/additional-online-account/`+id+`/edit"><i class="fas fa-pen"></i></a>
                            <a class="btn-circle text-danger" href="/package/additional-online-account/delete/`+id+`" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                        </div>
                    `
                    );
            },
        },
    ],
    searchDelay: 750,
    buttons: [],
    columnDefs: [
        {
            defaultContent: "-",
            targets: "_all",
            className: "text-left",
            },
        ],
    });
$(".dataTables_filter").hide();

$("#btnSearch").click(function name() {
    let valInput = document.getElementById("myInputTextField").value;
    dataTable.search(valInput).draw();
});

// Add event listener for opening and closing details
$('#table-additional-online-account tbody').on('click', '#btnExpand', function(){
    var tr = $(this).closest('tr');
    var row = dataTable.row( tr );
    var id = row.data().id;

    if(row.child.isShown()){
        document.querySelector('.buttonExpand'+id).innerHTML = '<span class="expand'+id+'">Klik untuk detail</span>';
        row.child.hide();
        tr.removeClass('shown');
    } else {
        document.querySelector('.buttonExpand'+id).innerHTML = '<span class="expand'+id+'">Tutup untuk detail</span>';
        row.child(format(row.data())).show();
        tr.addClass('shown');
    }
});

function format ( d ) {
    return d.details;
}