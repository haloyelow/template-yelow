const token = $('meta[name="csrf-token"]').attr('content')
const routeSendEmail = $("#routeSendEmail").val()
const routeVerifyUser = $("#routeVerifyUser").val()
const routeGetData = $("#routeGetData").val()

displayData()

function displayData(){
   var dataTable = $("#table-user").DataTable({
      language: {
         paginate: {
               next: '<i class="fas fa-chevron-right"></i>',
               previous: '<i class="fas fa-chevron-left"></i>'
         }
      },
      processing: false,
      bLengthChange: false,
      bInfo: false,
      pageLength: 10,
      stripeClasses: ['stripe-color', 'stripe-2'],
      ajax: {
         url: "/account-management",
      },
      columns: [
         { data: "email" },
         { data: "createdTime" },
         { data: "role" },
         { data: "status" },
         {
               data: "action",
               orderable: false,
               searchable: false,
               render: function (data, type, row) {
                  var id = row.idUser;
                  var tokenUser = row.newToken;
                  return (
                     `
                        <div class="d-flex">
                           <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="edit(event)"><i class="fas fa-pen"></i></a>
                           <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                        </div>
                     `
                  );
               },
         },
      ],
      searchDelay: 750,
      buttons: [],
      columnDefs: [
         {
               defaultContent: "-",
               targets: "_all",
               className: "text-left",
         },
      ],
      bDestroy: true,
      // order: [[5, "ASC"]],
   });

   $(".dataTables_filter").hide();
   
   $("#btnSearch").click(function name() {
      let valInput = document.getElementById("myInputTextField").value;
      dataTable.search(valInput).draw();
   });
}

$('#btnAdd').on('click', function (e) {
   $('#modalForm').modal('show') 
   document.title = 'Add Account'
});

$('#modalForm').on('hidden.bs.modal', function () {
   $('#fieldEmail').val('')
   $('#fieldRole').val('')
   document.title = 'Account Management'
   document.getElementById('myModalLabel').innerHTML = 'Add Account'
});

$('#fieldRole').on('change', function(e){
   $('#btnSubmit').prop("disabled", false)
   $('#btnSubmit').focus()
})

function edit(ev) {
   document.getElementById('myModalLabel').innerHTML = 'Edit Account'
   document.title = 'Edit Account'
   $('#modalForm').modal('show')
   
   ev.preventDefault();
   let idUser = ev.currentTarget.getAttribute('id');
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeGetData,
      data: {id: idUser},
      success: function (response) {
         $('.notifPassword').show()
         $('#idEdit').val(idUser)
         $('#fieldEmail').val(response.email)
         $('#fieldRole').val(response.roleName)
      }
   })
}

$('#btnSubmit').on('click', function (e) {
   e.preventDefault()
   let idUser = $('#idEdit').val()
   
   if(idUser.length >= 7){
      var url = "/account-management/update"
      var data = {
         email:$("#fieldEmail").val(),
         role:$("#fieldRole").val(),
         token:token,
         id:idUser
      } 
   }else{
      var url = "/account-management/save"
      var data = {
         email:$("#fieldEmail").val(),
         role:$("#fieldRole").val(),
         token:token,
      }
   }
   
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: url,
      data: data,
      success: function (response) {
         console.log(response.token)
         document.getElementById('myModalLabel').innerHTML = 'Add Account'
         document.title = 'Account Management'
         $('#idEdit').val('')
         
         var request = {
            uuid:response.data.uuid,
            name:response.data.name,
            email:response.data.email,
            token:response.token
         }
         if(idUser.length < 7){
            sendEmail(request)
         }
         document.getElementById("formInput").reset()
         $('#modalForm').modal('hide') 
         const notifAlert = `
                              <div class="col-md-12 div`+response.data.uuid+`">
                                 <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                              </div>
                           `
         $('#notif').append(notifAlert)
         setTimeout(function() {
               $('.div'+response.data.uuid).fadeOut('slow')
         }, 1500)
         displayData()
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});

function sendEmail(params){
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeSendEmail,
      data: params,
      success: function (response) {
         console.log(response)
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })
}

function hapus(ev) {
   ev.preventDefault();
   let idUser = ev.currentTarget.getAttribute('id');
   swal({
      title: "Are you sure want to delete this data?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
   })
   .then((willDelete) => {
      if (willDelete) {
         $.ajax({
               headers: { 'X-CSRF-TOKEN': token },
               type: 'GET',
               url: "/account-management/delete/",
               data: {id: idUser},
               success: function (response) {
                  const notifAlert = `
                           <div class="col-md-12 div`+response.data.uuid+`">
                              <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                           </div>
                     `
                  $('#notif').append(notifAlert)
                  setTimeout(function() {
                     $('.div'+response.data.uuid).fadeOut('slow')
                  }, 1500)
                  displayData()
               }
         })
      } else {
         swal("Hapus gagal.");
      }
   });
}