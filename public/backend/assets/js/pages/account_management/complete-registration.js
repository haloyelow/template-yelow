const token = $('meta[name="csrf-token"]').attr('content')
const routeSave = $("#routeSave").val()
const url = new URL(window.location.href)
const tokenUser = url.searchParams.get("token");

$('#btnSubmit').on('click', function (e) {
   e.preventDefault()

   var data = {
      token:tokenUser,
      name:$("#name").val(),          
      password:$("#password").val()          
   }

   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeSave,
      data: data,
      success: function (response) {
         if(response.alert == 'success'){
            $('#fieldForm').hide()
            document.getElementById("formInput").reset()
            const notifAlert = `
            <div class="col-md-12 div`+response.data.uuid+`">
            <div class="alert alert-`+response.alert+`">`+response.message+`</div>
            <div class="alert alert-`+response.alert+`">`+response.message_two+`</div>
            </div>
            `
            $('#notif').append(notifAlert)
            setTimeout(function() {
               $('.div'+response.data.uuid).fadeOut('slow')
               deleteToken(tokenUser)
            }, 2000)
         }else{
            $('#fieldForm').hide()
            document.getElementById("formInput").reset()
            const notifAlert = `
                                 <div class="col-md-12 div`+response.alert+`">
                                    <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                                 </div>
                              `
            $('#notif').append(notifAlert)
            setTimeout(function() {
               $('.div'+response.alert).fadeOut('slow')
            }, 2000)
         }

         setTimeout(function() {
            window.close()
         }, 3000)

      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});

function deleteToken(params){
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: '/delete-token',
      data: { 'token': params },
      success: function (response) {
         console.log(response)
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })
}