const token = $('meta[name="csrf-token"]').attr('content')
const routeSaveData = $("#routeSaveData").val()

$(function () {
    $("#fieldDiskon").hide()
    $('.btnSubmit').prop("disabled", true)    
    $('#button_add_details').prop("disabled", true)    

    var switchStatus = false
    $("#switch5").on('change', function () {
        if ($(this).is(':checked')) {
            switchStatus = $(this).is(':checked')
            $("#fieldDiskon").show()
            $('#isiDiskon').on('change', function (e) {
                if($(this).val() > 100){
                    swal({
                        title: "Maximum value is 100%",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    })
                    $('#isiDiskon').val(0)
                }
            })
            $('#endDiscount').on('change', function (e) {
                if( $(this).val() <= $('#startDiscount').val()){
                    swal({
                        title: "Invalid date!",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Re-arranged date",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    })
                    $('#endDiscount').val('')
                } 
            })
        }
        else {
            switchStatus = $(this).is(':checked')
            $("#fieldDiskon").hide()
            $('#isiDiskon').val('')
            $('#startDiscount').val('')
            $('#endDiscount').val('')
        }
    });

    $('#price').on('change', function(e){
        if($(this).val() != ''){
            $('#button_add_details').prop("disabled", false)
        }
    })

    $('#input_details_0').on('change', function(e){
        if($('#name').val() != '' && $('#price').val() != '' && $(this).val() != ''){
            $('.btnSubmit').prop("disabled", false)
        }
    })
    
    // Repeater field description
    $('#button_add_details').on('click', function (e) {
        e.preventDefault();
        var indexDetail = 0;
        $('#list_detail').children().each(function () {
            var index = $(this).attr('data-index-detail');
            if (index > indexDetail) {
                indexDetail = index;
            }
        });
        indexDetail = Number(indexDetail) + 1;
        const newDetail = `
            <div class="inner-repeater mb-3" data-index-detail="${indexDetail}">
                <div data-repeater-list="inner-group" class="inner mb-3">
                    <div data-repeater-item class="inner mb-3 row">
                        <div class="col-md-8 col-8 d-flex h-100 align-items-center">
                            <div style="border-radius: 100px; height: 25px; width: 25px; text-align: center; display: flex; justify-content: center; align-items: center; background-color: #F4AE00; color: #fff;" class="me-2">
                                <i class="fas fa-check"></i>
                                    </div>
                            <input type="text" name="details[${indexDetail}]" id="input_details_${indexDetail}" class="inner form-control" placeholder="Enter here..."/>
                        </div>
                        <div  class="col-md-1 col-4">
                            <div class="d-grid ">
                                <button type="button" data-type="btn-remove" data-index-detail="${indexDetail}" class="btn btn-danger " ><i class="fa fa-trash "></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `;
            $('#list_detail').append(newDetail);
            $('#input_details_'+indexDetail).focus()
            $('#input_details_'+indexDetail).on('change', function(e){
                if($(this).val() != ''){
                    $('.btnSubmit').prop("disabled", false)
                }
            })
        });

        //  Event button remove
        $(document).on('click', "button[data-type='btn-remove']", function (e) {
            e.preventDefault();
            let indexDetail = $(this).attr('data-index-detail');
            if ($('#list_detail').children().length !== 1) {
                $(`div[data-index-detail="${indexDetail}"]`).remove();
        }
    });
    
    $('.btnSubmit').on('click', function (e) {
        $('#modalIsiEmail').modal('show') 
    });

    $('#modalIsiEmail').on('hidden.bs.modal', function () {
        $('#fieldEmail').val('')
        $('#fieldPassword').val('')
        $('#fieldPassword').prop("disabled", true)
    });

    $('#fieldEmail').on('focusout', function (e) {
        const token = $('meta[name="csrf-token"]').attr('content')
        const routeCheckUserEmail = $("#routeCheckUserEmail").val()
        event.preventDefault();
        
        let email = $('#fieldEmail').val();

        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: routeCheckUserEmail,
            data:{
                email:email
            },
            success: function (data) {
                let hasil = data

                if(hasil == true){
                    $('#fieldPassword').prop("disabled", false)
                    $('#fieldPassword').focus()
                }else{
                    $('#errorEmail').show()
                    $('#fieldPassword').prop("disabled", true)
                    setTimeout(function() {
                        $('#errorEmail').fadeOut('slow');
                    }, 1500);
                }
            },
            error: function(error) {
                $('#fieldPassword').prop("disabled", true)
            }
        })
    });
    
    $('#fieldPassword').on('focusout', function (e) {
        const token = $('meta[name="csrf-token"]').attr('content')
        const routeCheckUserPassword = $("#routeCheckUserPassword").val()
        event.preventDefault();
        
        let email = $('#fieldEmail').val();
        let password = $('#fieldPassword').val();

        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: routeCheckUserPassword,
            data:{
                email:email,
                password:password
            },
            success: function (data) {
                let hasil = data

                if(hasil == true){
                    $('#btnSubmitFinal').prop("disabled", false)
                    $('#btnSubmitFinal').focus()
                }else{
                    $('#errorPassword').show()
                    $('#btnSubmitFinal').prop("disabled", true)
                    setTimeout(function() {
                        $('#errorPassword').fadeOut('slow');
                    }, 1500);
                }
            },
            error: function(error) {
                $('#btnSubmitFinal').prop("disabled", true)
            }
        })
    });

    $('#btnSubmitFinal').on('click', function (e) {
        e.preventDefault()
        var url = routeSaveData
        var loopDetail = []
        var indexDetail = 0
        $('#list_detail').children().each(function () {
            var index = $(this).attr('data-index-detail')
            if (index > indexDetail) {
                indexDetail = index
            }
            loopDetail.push($('#input_details_'+indexDetail).val())
        })
        var data = {
                    name:$("#name").val(),
                    price:$("#price").val(),
                    discount:$("#isiDiskon").val(),
                    start_discount:$("#startDiscount").val(),
                    end_discount:$("#endDiscount").val(),
                    details:loopDetail,
            } 

        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: url,
            data: data,
            success: function (response) {
                console.log(response.message)
                window.location = `/package/helpdesk`;
            },
            error: function(xhr) {
                var err = JSON.parse(xhr.responseText)
                var errorString = '<ul>'
                $.each( err.errors, function( key, value) {
                    errorString += '<p>' + value + '</p>'
                })
                errorString += '</ul>'
                
                swal({
                    type: "warning",
                    title: errorString,
                    showCancelButton: false,
                    confirmButtonColor: "#e3342f",
                    confirmButtonText: "Ok",
                })
            }
        })    
    });
    
});
